<?php

/**
 * TopNews form.
 *
 * @package    SupLan
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TopNewsForm extends BaseTopNewsForm
{
  public function configure()
  {
  	$this->widgetSchema['image'] = new sfWidgetFormSelect(array('choices'=>$this->getImagesGames()));
  	unset($this['created_at'], $this['updated_at'], $this['slug']);
  }
  public function getImagesGames(){
  	$array_images = array();
  	$dirname = sfConfig::get('sf_web_dir').'/images/topnews/';
  	$dir = opendir($dirname);
  	
  	while($file = readdir($dir)) {
  		if($file != '.' && $file != '..' && !is_dir($dirname.$file))
  		{
  			$array_images['topnews/'.$file]=$file;
  		}
  	}
  	
  	closedir($dir);
  	return $array_images;
  }
}
