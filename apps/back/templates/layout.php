<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php include_http_metas() ?>
<?php include_metas() ?>
<?php include_title() ?>
<link rel="shortcut icon" href="/favicon.ico" />
<?php include_stylesheets() ?>
<?php include_javascripts() ?>
</head>
<body style="width:90%">
<div id="global">
  <div id="header" class="clear all_col" style="cursor:pointer;"></div>
  <div class="navbar clear all_col">
    <div class="navbar-inner" style="border-radius: 0">
      <div class="container">
        <div class="nav-collapse">
          <ul class="nav">
					<li><a href="<?php echo url_for('@news') ?>">News</a></li>
					<li><a href="<?php echo url_for('@top_news') ?>">Top News</a></li>
					<li><a href="<?php echo url_for('@step') ?>">Steps</a></li>
					<li><a href="<?php echo url_for('@game') ?>">Games</a></li>
					<li><a href="<?php echo url_for('@round') ?>">Rounds</a></li>
					<li><a href="<?php echo url_for('@team') ?>">Teams</a></li>
					<li><a href="<?php echo url_for('@player') ?>">Players</a></li>
					<li><a href="<?php echo url_for('@payment') ?>">Payments</a></li>
          <li><a href="<?php echo url_for('@cms') ?>">CMS</a></li>
          <li><a href="<?php echo url_for('@stream') ?>">Streams</a></li>
					<li><a href="<?php echo url_for('@recap') ?>">Récapitulatifs</a></li>
          <?php
            if($sf_user->isAuthenticated()){
          ?>
          <li><a href="<?php echo url_for('@sf_guard_signout') ?>" title="<?php echo $sf_user->getUsername() ?>">Déconnexion</a></li>
          <?php
            } else  {
          ?>
          <li><a href="<?php echo url_for('@index_login') ?>">Connexion</a></li>
          <?php
            }
          ?>
				</ul>
        </div>
      </div>
    </div>
  </div>
  <div id ="content">
				<?php echo $sf_content ?>
    <div class="clear"></div>
  </div>
  <div id="footer">
    <p style="margin-top:10px;padding:0;">&copy; 2012 SupLan - Tous droits réservés</p>
  </div>
</div>
<script type="text/javascript">
  $('#header').click(function(){
    $(window).attr('location','<?php echo url_for('@homepage') ?>');
  });
</script>
</body>
</html>