<?php

/**
 * cms actions.
 *
 * @package    SupLan
 * @subpackage cms
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class cmsActions extends sfActions
{
  public function executeShow(sfWebRequest $request)
  {
  	$this->cms = $this->getRoute()->getObject();
  }
}
