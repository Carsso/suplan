<?php

/**
 * index actions.
 *
 * @package    SupLan
 * @subpackage ajax
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ajaxActions extends sfActions
{
	/**
	 * Executes equipe action
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeEquipe(sfWebRequest $request)
	{
		$round_id=$request->getParameter('round');
		$equipe_slug=strtolower(Doctrine_Inflector::urlize($request->getParameter('name')));
		$round = Doctrine::getTable('Round')->find($round_id);
		$this->team = Doctrine_Query::create()
		->from('Team')
		->where('slug = ?',$round_id.'-'.$equipe_slug)
		->fetchOne();
		$reserved=false;
		if(is_object($this->team)){
			foreach($this->team->getPlayer() as $player){
				if(count($player->getPayment())){
					$reserved=true;
				}
			}
			if(!$reserved){
				foreach($this->team->getPlayer() as $player){
					$player->delete();
				}
				$this->team->delete();
				$this->team = Doctrine_Query::create()
				->from('Team')
				->where('slug = ?',$round_id.'-'.$equipe_slug)
				->fetchOne();
			}
		}
		if(is_object($this->team)){
			echo $this->team->getName();
		} elseif(count($round->getTeamReserved()) >= $round->getGame()->getMaxNbTeams()){
			echo '-1';
		} else {
			echo '0';
		}
		die();
	}

	/**
	 * Executes equipeLogin action
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeEquipeLogin(sfWebRequest $request)
	{
		sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
		$round_id=$request->getParameter('round');
		$equipe_slug=strtolower(Doctrine_Inflector::urlize($request->getParameter('name')));
		$round = Doctrine::getTable('Round')->find($round_id);
		$this->team = Doctrine_Query::create()
		->from('Team')
		->where('slug = ?',$round_id.'-'.$equipe_slug)
		->fetchOne();
		if(is_object($this->team)){
			if($this->team->getPassword() == $request->getParameter('password')){
				$this->err = -1;
				$this->getUser()->setAttribute('team_pwd',$this->team->getPassword());
			} else {
				$this->err = 1;
				$this->getUser()->setAttribute('team_pwd',null);
			}
		} else{
			if(count($round->getTeamReserved()) >= $round->getGame()->getMaxNbTeams()){
					$this->err = 3;
			} else {
				if($request->getParameter('password') == $request->getParameter('password2')){
					$this->err = -1;
					$this->team = new Team();
					$this->team->setPassword($request->getParameter('password'))
					->setName($request->getParameter('name'))
					->setRoundId($round_id)
					->save();
					$this->getUser()->setAttribute('team_pwd',$this->team->getPassword());
				} else {
					$this->err = 2;
					$this->getUser()->setAttribute('team_pwd',null);
				}
			}
		}
  	echo ($this->err == -1) ? url_for('participation_joueurs',$this->team) : $this->err;
  	
  	die();
	}
	/**
	 * Executes players action
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeEquipeUsers(sfWebRequest $request)
	{
		$this->result = 0;
		$equipe_action = $request->getParameter('equipe_action',null);
		$prenom = misc::ucname($request->getParameter('prenom',null));
		$nom = mb_strtoupper($request->getParameter('nom',null),'UTF-8');
		$pseudo = $request->getParameter('pseudo',null);
		$email = $request->getParameter('email',null);
		$team_id = $request->getParameter('id',null);
		$team_pwd = $this->getUser()->getAttribute('team_pwd',null);
		$this->team = Doctrine::getTable('Team')->find($team_id);
		if((is_object($this->team)) && ($team_pwd == $this->team->getPassword())){
			if($equipe_action=="add"){
				if($prenom && $nom){
					if(count($this->team->getPlayer())<$this->team->getRound()->getGame()->getTeamNbPlayers()){
						$player = new Player();
						$player->setFirstName($prenom)
							->setLastName($nom)
							->setPseudo($pseudo)
							->setEmail($email)
							->setTeam($this->team)
							->save();
						$this->result = 1;
					} else {
						$this->result = 0;
					}
				} else {
					$this->result = -1;
				}
			} elseif($equipe_action=="update"){
				$user_id = $request->getParameter('user',null);
				$this->player = Doctrine::getTable('Player')->find($user_id);
				if($this->player->getTeam()->getId() == $team_id){
					if($prenom && $nom){
						$this->player->setFirstName($prenom)
							->setLastName($nom)
							->setPseudo($pseudo)
							->setEmail($email)
							->save();
						$this->result = 1;
					} else {
						$this->result = -1;
					}
				} else {
					$this->forward404();
				}
			} else {
				$this->forward404();
			}
		}
		echo $this->result;
		
		die();
	}
}
