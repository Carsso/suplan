<?php

/**
 * steps actions.
 *
 * @package    SupLan
 * @subpackage steps
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class stepsActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
  }
  
  public function executeShow(sfWebRequest $request)
  {
  	$this->step = $this->getRoute()->getObject();
  }
  
  public function executeList(sfWebRequest $request)
  {
  	$this->step = $this->getRoute()->getObject();
  	$this->rounds = $this->step->getRound();
  }
}
