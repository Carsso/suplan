<div class="boxed">
<h2>Choix de l'Equipe</h2>
<p class="big" style="margin-left: 10px;">
	Etape: <?php echo $round->getStep() ?> -
	Jeu: <?php echo $round->getGame() ?>
</p>
<img
	src="<?php echo image_path($round->getGame()->getImage()) ?>"
	alt="<?php echo $round->getGame()->getName() ?>"
	title="<?php echo $round->getGame()->getName() ?>"
	style="height: 100px; width: 100px; margin: 10px; float: left;" />
<div style="width: 640px; margin: 10px; float: left;">
<?php if(count($round->getTeamReserved()) >= $round->getGame()->getMaxNbTeams()): ?>
<div class="alert alert-error">
ATTENTION: Ce jeu est complet pour cette étape.<br />
Il n'est désormais plus possible d'inscrire une équipe.
</div>
<?php endif; ?>
	<p style="margin-left: 10px;">
		Entrez votre nom d'équipe (Prénom Nom si vous jouez en solo):<br /> <input
			type="text" id="team-name-input" name="team" style="width: 400px;" /><br />
		<span id="team-name-txt" style="font-size: 10px;"></span>
	</p>
	<p id="old_and_new_team" style="display: none; margin-top: 8px;margin-left: 10px;">
		Mot de passe de l'équipe: <input type="password" id="password-input"
			name="password" />
	</p>
	<p id="new_team" style="display: none; margin-top: 8px;margin-left: 10px;">
		Confirmation du mot de passe: <input type="password"
			id="password2-input" name="password2" />
	</p>
	<p style="margin-top: 8px;margin-left: 10px;">
		<input style="display: none;" id="team-form-submit-btn" type="button">
	</p>
</div>
<div style="clear: both;"></div>
<script type="text/javascript">
	var xhr;
	var waiting_time;
	$(document).ready(function(){
		$('#team-name-input').keyup(function(){
				$('#team-name-input').css('background','url("/images/loading.gif") no-repeat scroll 384px center white'); 
                clearTimeout( waiting_time );
                waiting_time = setTimeout(function() {
                        updateAjx();
                }, 500);
        });
        $('#team-form-submit-btn').click(function(){
        	$('#team-form-submit-btn').attr('value','Envoi...');
        	$.ajax({
              	url: "<?php echo url_for('@ajax_team_login?round='.$round->getId().'&name=') ?>"+$('#team-name-input').val()+"?password="+$('#password-input').val()+"&password2="+$('#password2-input').val(),
	              cache: false,
	              success: function(html){
	              	if(html=='0'){
	                	$('#team-form-submit-btn').attr('value','Echec ! - Cliquez pour réessayer');
		            } else {
		            	if(html=='1'){
		                	$('#team-form-submit-btn').attr('value','Mot de passe incorrect ! - Cliquez pour réessayer');
			            } else {
			            	if(html=='2'){
			                	$('#team-form-submit-btn').attr('value','Les mots de passe ne correspondent pas ! - Cliquez pour réessayer');
				            } else {
				            	if(html=='3'){
				                	$('#team-form-submit-btn').attr('value','Désolé, ce jeu est complet pour cette étape !');
				                	alert('Désolé, ce jeu est complet pour cette étape.	Il n\'est désormais plus possible d\'inscrire une équipe.');
					            } else {
					            	$('#team-form-submit-btn').unbind('click');
				                	$(window).attr('location',html);
					            }
				            }
			            }
		           	}
	              },
	            	error: function(){
	                	$('#team-form-submit-btn').attr('value','Echec ! - Cliquez pour réessayer');
	            	}
	            });
        });
	});
	function updateAjx(){
        if(typeof(xhr)!='undefined'){
                xhr.abort(); 
        }
        $('#team-name-input').css('background','url("/images/loading.gif") no-repeat scroll 384px center white'); 
		if($('#team-name-input').val() && $('#team-name-input').val()!=''){
			xhr = $.ajax({
              	url: "<?php echo url_for('@ajax_team_status?round='.$round->getId().'&name=') ?>"+$('#team-name-input').val(),
	              cache: false,
	              success: function(html){
	              	if(html=='-1'){
	              		$('#team-name-input').css('background','url("/images/cross.png") no-repeat scroll 384px center #F2BABA');
	              		$('#team-name-txt').html('Désolé, ce jeu est complet pour cette étape.	Il n\'est désormais plus possible d\'inscrire une équipe.');
	              		$('#old_and_new_team').hide();
			            	$('#new_team').hide();
			            	$('#team-form-submit-btn').attr('value','FAIL');
			            	$('#team-form-submit-btn').hide();
	              	} else {
		              	if(html=='0'){
				            	$('#team-name-input').css('background','url("/images/tick.png") no-repeat scroll 384px center #E6FFB8');
				            	$('#team-name-txt').html('Nom d\'équipe disponible - Choisissez un mot de passe pour l\'équipe.');
				            	$('#old_and_new_team').show();
				            	$('#new_team').show();
				            	$('#team-form-submit-btn').attr('value','Créer');
				            	$('#team-form-submit-btn').show();
				            } else {
			              		$('#team-name-input').css('background','url("/images/cross.png") no-repeat scroll 384px center #FFE2A1');
			              		$('#team-name-txt').html('Nom d\'équipe déjà pris - Si vous en faites parti, entrez le mot de passe.');
			              		$('#old_and_new_team').show();
					            	$('#new_team').hide();
					            	$('#team-form-submit-btn').attr('value','Accèder');
					            	$('#team-form-submit-btn').show();
				           	}
	              	}
	              }
	            });
			} else {
				$('#team-name-input').css('background','white');
				$('#team-name-txt').html('');
            	$('#old_and_new_team').hide();
            	$('#new_team').hide();
            	$('#team-form-submit-btn').hide();
            	$('#team-form-submit-btn').attr('value','');
			}
	}
</script>
</div>