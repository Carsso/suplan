<?php

/**
 * news actions.
 *
 * @package    SupLan
 * @subpackage news
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class newsActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->newss = Doctrine_Query::create()
    	->from('News')
    	->where('is_visible =  1')
    	->orderBy('updated_at DESC')
      ->limit(5)
      ->execute();
    $this->topnewss = Doctrine_Query::create()
    	->from('TopNews')
    	->orderBy('updated_at DESC')
      ->limit(5)
      ->execute();
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->news = $this->getRoute()->getObject();
  }
}
