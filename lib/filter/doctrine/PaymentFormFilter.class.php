<?php

/**
 * Payment filter form.
 *
 * @package    SupLan
 * @subpackage filter
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PaymentFormFilter extends BasePaymentFormFilter
{
  public function configure()
  {
  	unset($this['slug']);
	  $this->widgetSchema['round_complete'] = new sfWidgetFormDoctrineChoice(array('model' => 'Payment', 'method' => 'getRoundComplete', 'key_method' => 'getRoundId', 'add_empty' => true));
	  $this->validatorSchema['round_complete'] = new sfValidatorPass(array('required' => false));
	  $this->widgetSchema['team_complete'] = new sfWidgetFormDoctrineChoice(array('model' => 'Payment', 'method' => 'getTeamComplete', 'key_method' => 'getTeamId', 'add_empty' => true));
	  $this->validatorSchema['team_complete'] = new sfValidatorPass(array('required' => false));
	  $this->widgetSchema['player_complete'] = new sfWidgetFormDoctrineChoice(array('model' => 'Payment', 'method' => 'getPlayerComplete', 'key_method' => 'getPlayerId', 'add_empty' => true));
	  $this->validatorSchema['player_complete'] = new sfValidatorPass(array('required' => false));
	  $this->widgetSchema['email_paypal'] = new sfWidgetFormDoctrineChoice(array('model' => 'Payment', 'method' => 'getEmailPaypal', 'key_method' => 'getEmailPaypal', 'add_empty' => true));
	  $this->validatorSchema['email_paypal'] = new sfValidatorPass(array('required' => false));
    
  }
  
  protected function addRoundCompleteColumnQuery(Doctrine_Query $query, $field, $value){
  	$ra = $query->getRootAlias();
  	$query->innerJoin($ra . '.Player pa')
  	->innerJoin('pa.Team ta')
  	->innerJoin('ta.Round rnda')
  	->addWhere('rnda.id = ?', $value);
  		return $query;
  }
  
  protected function addTeamCompleteColumnQuery(Doctrine_Query $query, $field, $value){
  	$ra = $query->getRootAlias();
  	$query->innerJoin($ra . '.Player pb')
  	->innerJoin('pb.Team tb')
  	->addWhere('tb.id = ?', $value);
  	return $query;
  }
  
  protected function addPlayerCompleteColumnQuery(Doctrine_Query $query, $field, $value){
  	$ra = $query->getRootAlias();
  	$query->innerJoin($ra . '.Player pc')
  	->addWhere('pc.id = ?', $value);
  	return $query;
  }
}
