<?php

/**
 * Payment form.
 *
 * @package    SupLan
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PaymentForm extends BasePaymentForm
{
  public function configure()
  {
  	unset($this['created_at'], $this['updated_at'], $this['slug']);
  }
}
