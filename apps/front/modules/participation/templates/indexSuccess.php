<div class="boxed">
<h2>Choix du jeu</h2>
<p class="big" style="margin-left: 10px;">Etape: <?php echo $step ?></p>
<?php 
$i=0;
foreach ($rounds as $round){
	$i++;
	?>
<a href="<?php echo url_for('participation_equipe',$round) ?>"> <img
	src="<?php echo image_path($round->getGame()->getImage()) ?>"
	alt="<?php echo $round->getGame()->getName() ?>"
	title="<?php echo $round->getGame()->getName() ?>"
	style="height: 150px; width: 150px; margin: 10px; float: left;" />
</a>
<?php
}
if(!$i) {
	?>
<div class="alert alert-error">Désolé, impossible de s'inscrire pour cette étape actuellement !</div>
<?php
}
?>
<div style="clear:both;"></div>
</div>