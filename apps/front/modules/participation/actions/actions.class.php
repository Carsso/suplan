<?php

/**
 * participation actions.
 *
 * @package    SupLan
 * @subpackage participation
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class participationActions extends sfActions
{
	/**
	 * Executes index action
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeIndex(sfWebRequest $request)
	{
		if($this->step = $this->getRoute()->getObject()){
			if($this->step->getStatus()==1){
				$step_id = $this->step->getId();
				$this->rounds = Doctrine_Query::create()
				->from('Round')
				->where('step_id = ?',$step_id)
				->execute();
			} else {
				$this->rounds = array();
			}
		} else {
			$this->rounds = array();
		}
	}
	/**
	 * Executes equipe action
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeEquipe(sfWebRequest $request)
	{
		$this->round = $this->getRoute()->getObject();
	}
	/**
	 * Executes equipe action
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeJoueurs(sfWebRequest $request)
	{
		$this->team =  $this->getRoute()->getObject();
		$team_pwd = $this->getUser()->getAttribute('team_pwd',null);
		$payment = $request->getParameter('payment');
		if($payment){
			if($payment == 'error')
				$this->payment = '<div class="alert alert-error">Echec du paiement</div>';
			else
				$this->payment = '<div class="alert alert-success">Paiement réussi</div>';
		} else {
			$this->payment = false;
		}
		if($team_pwd == $this->team->getPassword()){
			$this->maxplayers = $this->team->getRound()->getGame()->getTeamNbPlayers();
			$this->players = $this->team->getPlayer();
		} else {
			sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
			$this->redirect(url_for('participation_equipe',$this->team->getRound()));
		}
	}

	/**
	 * Executes paiement action
	 *
	 * @param sfRequest $request A request object
	 */
	public function executePaiement(sfWebRequest $request)
	{
		$player = $this->getRoute()->getObject();
		$team = $player->getTeam();
		$round = $team->getRound();
		if((count($round->getTeamReserved()) >= $round->getGame()->getMaxNbTeams())&&(count($team->getPlayersPayed())<1)):
			
		else:
		$context = sfContext::getInstance();
		$context->getConfiguration()->loadHelpers(array('Text','Url','Asset'));
		$paypal = new sfMyMasterPaypal('20.00', 'Participation SupLan: 20€ | '.truncate_text($player->__toString(),25).' ('.truncate_text($team->__toString(),25).') | '.truncate_text($round->getGame()->getLittleName(),20).' ('.truncate_text($round->getStep()->getCity(),12).')');
		$paypal->setParameter('CUSTOM', $player->getId());
		$paypal->expressCheckout();
		endif;
	}

}
