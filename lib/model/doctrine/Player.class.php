<?php
/**
 * Player
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    SupLan
 * @subpackage model
 * @author     Carsso
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Player extends BasePlayer
{
	public function __toString()
	{
		return $this->getFormattedLastName().' '.$this->getFormattedFirstName();
	}
	
	function getFormattedLastName(){
		return mb_strtoupper($this->getLastName(),'UTF-8');
	}
	
	function getFormattedFirstName(){
		return misc::ucname($this->getFirstName());
	}
	
	public function getTeamName(){
		return $this->getTeam()->__toString();
	}
	public function getRoundName(){
		return $this->getTeam()->getRoundName();
	}
	
	public function getStep(){
		return $this->getRound()->getStep();
	}
	public function getGame(){
		return $this->getRound()->getGame();
	}
	public function getRound(){
		return $this->getTeam()->getRound();
	}
	
	public function getRoundSlug(){
	return $this->getRound()->getSlug();
	}
	public function getStepSlug(){
	return $this->getStep()->getSlug();
	}
	public function getGameSlug(){
	return $this->getGame()->getSlug();
	}
	public function getTeamSlug(){
	return $this->getTeam()->getSlug();
	}
	public function getPayed(){
		return $this->getPayment()->getPayed();
	}
}
