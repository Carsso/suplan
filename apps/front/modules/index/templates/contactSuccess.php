<div class="all_col boxed">
<h1>Contact</h1>
<div class="row">
  <div class="span6">
<?php if(!$hideform): ?>
    <form action="" method="post" class="form-horizontal" style="margin-top: 20px;">

      <div class="control-group">
        <label class="control-label">Votre nom:</label>
        <div class="controls">
          <input type="text" name="name" style="width:300px;" />
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">Votre adresse email:</label>
        <div class="controls">
          <input type="text" name="from" style="width:300px;" />
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">Sujet:</label>
        <div class="controls">
          <input type="text" name="subject" style="width:300px;" />
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">Message:</label>
        <div class="controls">
          <textarea name="content" style="width:300px;" rows="10"></textarea>
        </div>
      </div>
      <div class="control-group">
        <div class="controls">
          <input type="submit" value="Envoyer le message" />
        </div>
      </div>
    </form>
<?php else: ?>
Votre message à bien été envoyé, merci.
<?php endif; ?>
  </div>
  <div class="span3">
    <h3>Pensez à consulter la FAQ</h3>
    <p>Avant de nous contacter, pensez à consulter la FAQ, vous y trouverez les réponses aux questions les plus fréquemment posées.</p>
  </div>
</div>
</div>