<div class="right_col">
  <div class="boxed p_center" style="padding: 15px 5px">
    <?php if($step->getStatus()==1): ?>
    <p>
      <a href="<?php echo url_for('participation_choix_jeu',$step) ?>" class="btn btn-primary btn-larger">Inscription (20€/joueur)</a>
    </p>
    <?php endif; ?>
    <p style="margin-bottom: 0">
      <a href="<?php echo url_for('etape_participants',$step) ?>" class="btn btn-primary btn-larger">Participants</a>
    </p>
  </div>
  <div id="map" class="boxed p_center">
    <h2>Plan d'accès</h2>
    <?php echo sfOutputEscaper::unescape($step->getContentAccess()) ?>
  </div>
  <div class="boxed">
    <h2>Logements</h2>
    <div style="margin-left: 5px;text-align: justify;">
      <?php echo sfOutputEscaper::unescape($step->getContentHotel()) ?>
    </div>
  </div>
  </div>
<div class="left_col">
  <div class="boxed">
    <h2><?php echo $step->getName() ?></h2>
    <div style="margin-left: 5px;text-align: justify;">
      <?php echo sfOutputEscaper::unescape($step->getContent()) ?>
    </div>
  </div>
  <div class="boxed">
    <h2>Horaires</h2>
    <div style="margin-left: 5px;text-align: justify;">
      <?php echo sfOutputEscaper::unescape($step->getContentSchedule()) ?>
    </div>
  </div>
  <div class="boxed">
    <div class="titlenews">Les Jeux</div>
    <?php foreach($step->getRound() as $round): ?>
    <div style="width:64px;height:64px;float:left;margin: 0 10px;">
      <?php echo image_tag($round->getGame()->getImage(), array('width'=>'64px','height'=>'64px')) ?>
    </div>
    <p>
      <strong><?php echo $round->getGame()->getName() ?></strong><br />
      <?php if($round->getGame()->getTeamNbPlayers()==1): ?>
      <?php echo $round->getGame()->getMaxNbTeams() ?> joueurs
      <?php else: ?>
      <?php echo $round->getGame()->getMaxNbTeams() ?> équipes de <?php echo $round->getGame()->getTeamNbPlayers() ?> joueurs
      <?php endif; ?>
    </p>
    <div class="clear"></div>
    <hr />
    <?php endforeach; ?>
  </div>
</div>

<div class="clear"></div>
<?php if($step->getStatus()==1): ?>
<p style="text-align: center; padding-bottom: 40px; margin-top:20px;">
  <a href="<?php echo url_for('participation_choix_jeu',$step) ?>" class="btn btn-primary btn-larger">Inscription (20€/joueur)</a>
</p>
<?php endif; ?>