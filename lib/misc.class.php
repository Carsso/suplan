<?php

/*
 * To change this template, choose Tools | Templates
* and open the template in the editor.
*/

/**
 * Description of misc
 *
 * @author germain
 */
class misc {

	public static function ucname($string) {
		$string =ucwords(strtolower($string));

		foreach (array('-', '\'', ' ') as $delimiter) {
			if (strpos($string, $delimiter)!==false) {
				$string =implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
			}
		}
		return $string;
	}

	public static function flname($string) {
		$string = ucwords(strtolower($string));
		$i=false;
		foreach (array('-', '\'', ' ') as $delimiter) {
			if (strpos($string, $delimiter)!==false) {
				$string =implode($delimiter, array_map(array('self', 'flonly'), explode($delimiter, $string)));
				$i=true;
			}
		}
		if(!$i){
			$string =self::flonly($string);
		}
		return $string;
	}

	public static function flonly($string) {
		$string = strtoupper($string);
		return $string{0}.'.';
	}




	// Permet de vérifier que le jour est un jour travaillé
	public static function dateCheckWorking($date)
	{
		// Dimanche(0) ou Samedi(6)
		if(date('w',$date)==0||date('w',$date)==6)
		{
			return false;
		}
		$jour = date('d',$date);
		$mois = date('m',$date);
		$annee = date('Y',$date);
		if($jour == 1 && $mois == 1) return 1; // 1er janvier
		if($jour == 1 && $mois == 5) return 1; // 1er mai
		if($jour == 8 && $mois == 5) return 1; // 5 mai
		if($jour == 14 && $mois == 7) return 1; // 14 juillet
		if($jour == 15 && $mois == 8) return 1; // 15 aout
		if($jour == 1 && $mois == 11) return 1; // 1er novembre
		if($jour == 11 && $mois == 11) return 1; // 11 novembre
		if($jour == 25 && $mois == 12) return 1; // 25 décembre
		// Pâques
		$date_paques = @easter_date($annee);
		$jour_paques = date('d',$date_paques);
		$mois_paques = date('m',$date_paques);
		if($jour_paques == $jour && $mois_paques == $mois)
		{
			return false;
		}
		if($jour_paques+60*60*24 == $jour && $mois_paques == $mois) //lundi de paques
		{
			return false;
		}
		// Ascension
		$date_ascension = $date_paques+60*60*24*39;
		if(date('d',$date_ascension) == $jour && date('m',$date_ascension) == $mois)
		{
			return false;
		}
		// Pentecote
		$date_pentecote = $date_paques+60*60*24*50;
		if(date('d',$date_pentecote) == $jour && date('m',$date_pentecote) == $mois)
		{
			return false;
		}
		return true;
	}

}

?>
