
<div class="right_col">
  <div id="inscr_accueil" class="boxed p_center">
    <h2>Prochaine LAN</h2>
    <p class="big">
      Lyon - 25, 26, 27 & 28 Mai
    </p>
    <p style="margin-top:15px;">
      <a href="<?php echo url_for('@etape_emptyslug').'lyon' ?>" class="btn btn-primary btn-larger">Details et Inscription</a>
    </p>
  </div>
  <div id="map" class="boxed p_center">
    <h2>Plan d'accès</h2>
    <iframe src="http://www.map-generator.net/extmap.php?name=SUPINFO%20Lyon&amp;address=16%20rue%20Jean%20Desparmet%2069008%20Lyon&amp;width=276&amp;height=200&amp;maptype=map&amp;zoom=14&amp;hl=fr&amp;t=1334423724" frameborder="no" marginwidth="0" marginheight="0" scrolling="no" width="98%" height="200px"></iframe>
    <p class="big" style="margin-top:10px;margin-bottom:0;">
      <strong>SUPINFO Lyon</strong>
    </p>
    <p>
      16 rue Jean Desparmet<br />
      69008 Lyon
    </p>
  </div>
</div>
<div class="left_col">
  <div id="slide" class="boxed p_center">
    <div id="socials">
      <p>
        <a href="https://www.facebook.com/">
          <button class="zocial facebook icon" style="font-size: 18px;">Facebook</button>
        </a>
      </p>
      <p>
        <a href="https://twitter.com/">
          <button class="zocial twitter icon" style="font-size: 18px;">Twitter</button>
        </a>
      </p>
      <p>
        <a href="irc://irc.quakenet.org/">
          <button class="zocial googleplus icon" style="font-size: 18px;">Google+</button>
        </a>
      </p>
    </div>
    <div id="slider_nivo_parent">
      <div id="slider" class="nivoSlider theme-default">
        <?php foreach ($topnewss as $news): ?>
        <?php echo image_tag($news->getImage(), array('title'=>$news->getText())) ?>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
  <div id="lastnews" class="boxed">
    <h2>News</h2>
    <?php $sep = '' ?>
    <?php foreach ($newss as $news): ?>
    <h3 style="margin-left: 10px;">
      <a href="<?php echo url_for('news_show',$news) ?>"><?php echo $news->getTitle() ?></a>
    </h3>
    <p style="margin: 0 10px 10px 10px; text-align: justify;">
      <?php
      $news_string = strip_tags(sfOutputEscaper::unescape($news->getContent()));
      $minword = 4;
      $length = 380;
      $sub = '';
      $len = 0;

      foreach (explode(' ', $news_string) as $word)
      {
        $part = (($sub != '') ? ' ' : '') . $word;
        $sub .= $part;
        $len += strlen($part);

        if (strlen($word) > $minword && strlen($sub) >= $length)
        {
          break;
        }
      }

      $stringDisplay = $sub . (($len < strlen($news_string)) ? '...' : '');
      ?>
      <?php echo $stringDisplay ?>
    </p>
    <hr />
    <?php endforeach; ?>
  </div>
</div>
<div class="clear"></div>
<div id="partenaires" class="clear boxed all_col">
  <div class="partenaire"></div>
  <div class="partenaire"></div>
  <div class="partenaire"></div>
  <div class="partenaire"></div>
  <div class="partenaire"></div>
  <div class="partenaire"></div>
  <div class="partenaire"></div>
  <div class="partenaire"></div>
  <div class="partenaire"></div>
  <div class="partenaire"></div>
  <div class="partenaire"></div>
  <div class="clear"></div>
</div>
<script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
</script>