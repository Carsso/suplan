<div class="newsh"></div>
	<div class="newsc">
    <div class="titlenews"><?php echo ($cms)?$cms->getTitle():'Streaming' ?></div>
    <div class="texte">
      <?php echo ($cms)?sfOutputEscaper::unescape($cms->getContent()):'' ?>
      <div class="stream_btns">
        <?php
          $first = false;
          foreach($streams as $stream){
            if(!$first){
              $first = $stream;
            }
        ?>
          <div class="stream_div">
            <a href="<?php echo url_for('stream',$stream) ?>" target="stream_iframe" class="btn btn-primary"><?php echo $stream->getName() ?></a>
          </div>
        <?php
          }
        ?>
        <div class="clear"></div>
      </div>
      <div class="rounded" style="background-color:#000000">
        <iframe name="stream_iframe" src="<?php echo ($first)?url_for('stream',$first):'' ?>" width="730" height="360" frameborder="0" scrolling="no" style="margin:10px"></iframe>
      </div>
    </div>
  </div>
<div class="newsb"></div>
