<div class="boxibox" style="height: 540px; padding: 30px 50px;">
	<div id="carte" style="position:relative;">
		<div
			style="position: absolute; width: 100px; top: 368px; left: 540px;">
			<a class="link"
				href="<?php echo url_for('@etape_emptyslug').'nice' ?>">Nice<br />
			<span style="font-size: 10px;">20, 21, 22 avril</span>
			</a>
		</div>
		<div
			style="position: absolute; width: 100px; left: 384px; top: 360px;">
			<a class="link"
				href="<?php echo url_for('@etape_emptyslug').'montpellier' ?>">Montpellier<br />
			<span style="font-size: 10px;">24, 25, 26 fevrier</span>
			</a>
		</div>
		<div
			style="position: absolute; width: 100px; left: 459px; top: 282px;">
			<a class="link"
				href="<?php echo url_for('@etape_emptyslug').'lyon' ?>">Lyon<br />
			<span style="font-size: 10px;">13, 14, 15 janvier</span>
			</a>
		</div>
		<div style="position: absolute; width: 100px; top: 123px; left: 675px;">
			<a class="link"
				href="<?php echo url_for('@etape_emptyslug').'strasbourg' ?>">Strasbourg<br />
			<span style="font-size: 10px;">11, 12, 13 mai</span>
			</a>
		</div>

		<div style="position: absolute; width: 100px; left: 421px; top: 165px;">
			<a class="link"
				href="<?php echo url_for('@etape_emptyslug').'paris' ?>">Paris<br />
			<span style="font-size: 10px;">23, 24, 25 mars</span>
			</a>
		</div>
		<div
			style="position: absolute; width: 100px; left: 515px; top: 100px;">
			<a class="link"
				href="<?php echo url_for('@etape_emptyslug').'reims' ?>">Reims<br />
			<span style="font-size: 10px;">3, 4, 5 fevrier</span>
			</a>
		</div>
		<div style="position: absolute; width: 100px; top: 195px; left: 322px;">
			<a class="link"
				href="<?php echo url_for('@etape_emptyslug').'nantes' ?>">Nantes<br />
			<span style="font-size: 10px;">9, 10, 11 décembre</span>
			</a>
		</div>
		<div
			style="position: absolute; width: 100px; left: 198px; top: 132px;">
			<a class="link"
				href="<?php echo url_for('@etape_emptyslug').'rennes' ?>">Rennes<br />
			<span style="font-size: 10px;">2, 3, 4 décembre</span>
			</a>
		</div>
		<div
			style="position: absolute; width: 100px; left: 346px; top: 100px;">
			<a class="link"
				href="<?php echo url_for('@etape_emptyslug').'caen' ?>">Caen<br />
			<span style="font-size: 10px;">1, 2, 3 juin</span>
			</a>
		</div>
		<div
			style="position: absolute; width: 100px; right: 0px; bottom: 80px;">
			<a class="link"
				href="<?php echo url_for('@etapes_ics') ?>">
				<img src="<?php echo image_path('ical.gif') ?>" alt="ICAL" />
			</a>
		</div>
		<br>
	</div>
</div>
