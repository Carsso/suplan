<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php include_http_metas() ?>
<?php include_metas() ?>
<?php include_title() ?>
<link rel="shortcut icon" href="/favicon.ico" />
<?php include_stylesheets() ?>
<?php include_javascripts() ?>
</head>
<body>

<div id="global">
	<div id="header" class="clear all_col" style="cursor:pointer;"></div>
  <div class="navbar clear all_col">
    <div class="navbar-inner" style="border-radius: 0">
      <div class="container">
        <div class="nav-collapse">
          <ul class="nav">
            <li>
              <a href="<?php echo url_for('@homepage') ?>">Accueil</a>
            </li>
            <li>
              <a href="<?php echo url_for('@streams') ?>">Streaming</a>
            </li>
            <li>
              <a href="<?php echo url_for('@etape_emptyslug').'lyon' ?>">Inscription</a>
            </li>
            <li>
              <a href="<?php echo url_for('@etape_emptyslug').'lyon/participants' ?>">Participants</a>
            </li>
            <li>
              <a href="<?php echo url_for('@cms_page_emptyslug') ?>faq">FAQ</a>
            </li>
            <li>
              <a href="<?php echo url_for('@contact') ?>">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
	<div id ="content">
				<?php echo $sf_content ?>
    <div class="clear"></div>
	</div>
  <div id="footer">
    <p style="margin-top:10px;padding:0;">&copy; 2012 SupLan - Tous droits réservés</p>
  </div>
</div>
<script type="text/javascript">
  $('#header').click(function(){
    $(window).attr('location','<?php echo url_for('@homepage') ?>');
  });
</script>
</body>
</html> 