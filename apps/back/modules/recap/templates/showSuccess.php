<?php 

echo '<div style="margin:auto;width:980px;" id="step'.$step->getId().'">';
foreach($step->getRound() as $round){
	echo '<strong><u>Round: '.$round->__toString().' - '.count($round->getTeamReserved()).'/'.count($round->getGame()->getMaxNbTeams()).' Equipes réservées - '.($round->getGame()->getMaxNbTeams() - count($round->getTeamReserved())).' Places restantes</u></strong>';
	echo '<div style="margin-left:20px;" id="round'.$round->getId().'">';
	foreach($round->getTeam() as $team){
		echo '<div style="margin-left:20px;'.(($team->getSlotReserved())?'color:green':'color:red').'" id="team'.$team->getId().'">';
		echo '<strong>Equipe: '.$team->__toString().' - Id: '.$team->getId().' - Mot de passe: '.$team->getPassword().' - Réservée: '.($team->getSlotReserved()?'Oui':'Non').'</strong><br />';
		foreach($team->getPlayer() as $player){
			$payment = $player->getPayment();
			echo '<div style="margin-left:20px;'.(($payment->getPayed())?'color:green':(($team->getSlotReserved())?'color:orange':'color:red')).'" id="player'.$player->getId().'">';
			echo 'Joueur: '.$player->getPseudo().' - Id: '.$player->getId().' - Email: '.$player->getEmail().'<br />';
			echo '<div style="margin-left:20px;">';
			echo 'Prénom: '.$player->getFirstName().' - Nom: '.$player->getLastName().'<br />';
			echo 'Payé: '.(($payment->getPayed())?'Oui':'Non').' - Paypal: '.$payment->getEmailPaypal().'<br />';
			echo 'Transaction: '.$payment->getTransaction();
			echo '</div>';
			echo '</div>';
		}
		echo '</div>';
	}
	echo '</div>';
}
echo '</div>';

?>