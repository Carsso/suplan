<?php

/**
 * Cms form.
 *
 * @package    SupLan
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CmsForm extends BaseCmsForm
{
  public function configure()
  {
  	$this->widgetSchema['content'] = new sfWidgetFormTextareaTinyMCE();
  	unset($this['created_at'], $this['updated_at'], $this['slug']);
  }
}
