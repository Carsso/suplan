<?php

/**
 * index actions.
 *
 * @package    SupLan
 * @subpackage index
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class indexActions extends sfActions
{
  public function executeContact(sfWebRequest $request)
  {
  	$this->hideform=false;
  	if($request->isMethod('post')){
  		$admins = Doctrine_Query::create()->select('email_address ')->from('sfGuardUser')->where('is_super_admin = 1')->execute(array(), Doctrine::HYDRATE_SINGLE_SCALAR);
  		$destinataires=$admins;
	  	$subject = $request->getParameter('subject',null);
	  	$content = $request->getParameter('content',null);
	  	$expediteur = array($request->getParameter('from',null)=>$request->getParameter('name',null));
  		if($subject && $content && $expediteur){
  			$this->hideform=true;
	  		$context = sfContext::getInstance();
	  		$message = $context->getMailer()->compose($expediteur,
	  		$destinataires,
	  		'[SupLan-Contact] '.$subject,
	  		$content);
	  		$message = $message->setReplyTo($request->getParameter('from',null),$request->getParameter('name',null));
	  		$context->getMailer()->send($message);
  		}
  	}
  }
  public function executeError(sfWebRequest $request)
  {
  }
}
