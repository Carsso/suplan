<?php

/**
 * Stream form.
 *
 * @package    SupLan
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class StreamForm extends BaseStreamForm
{
  public function configure()
  {
    $this->widgetSchema['code'] = new sfWidgetFormTextarea(array(),array('rows'=>20,'cols'=>80));
    unset($this['created_at'], $this['updated_at'], $this['slug']);
  }
}
