<?php $many_payed = false ?>
<div class="boxed">
<h2>Choix des joueurs</h2>
<p class="big" style="margin-left: 10px;">
	Etape: <?php echo $team->getRound()->getStep() ?> -
  Jeu: <?php echo $team->getRound()->getGame() ?><br />
  Equipe: <?php echo $team ?>
</p>
<?php 
if($payment){
	echo sfOutputEscaper::unescape($payment);
}
?>
<h3 style="margin: 20px auto auto auto;width: 500px;text-align:left;">Joueurs:</h3>
<p style="margin: 20px auto auto auto;width: 500px;text-align:left;">
<?php
	$nb_j = 1;
	foreach($players as $player){
?> 
<input class="examplify_labels" type="text" style="width:180px"  id="player<?php echo $nb_j ?>-pseudo" value="<?php echo $player->getPseudo() ?>" title="Pseudo">
<input class="examplify_labels" type="text" style="width:280px" id="player<?php echo $nb_j ?>-email" value="<?php echo $player->getEmail() ?>" title="Email">
<br />
<input class="examplify_labels" type="text" style="width:180px"  id="player<?php echo $nb_j ?>-prenom" value="<?php echo $player->getFormattedFirstName() ?>" title="Prénom">
<input class="examplify_labels" type="text" style="width:160px"  id="player<?php echo $nb_j ?>-nom" value="<?php echo $player->getFormattedLastName() ?>" title="Nom">

<input type="button" id="player<?php echo $nb_j ?>-btn" value="Modifier">
	 &nbsp;&nbsp;
<?php
	 if((is_object($player->getPayment()) && ($player->getPayment()->getTransaction()))){
?>
<?php $many_payed = true ?>

   <input type="button" class="btn-success" value="Infos" onclick="alert('Paiement effectué le <?php echo format_date($player->getPayment()->getCreatedAt(),'D') ?> à <?php echo format_date($player->getPayment()->getCreatedAt(),'t').' par '.$player->getPayment()->getEmailPaypal() ?>');return false;">
	 <span class="label label-success">Place réservée</span>
<?php
	 } else {
?>
   <input type="button" value="Payer" onclick="$(window).unbind('beforeunload');$(window).attr('location','<?php echo url_for('participation_paiement', $player) ?>');"class="btn-primary">
    <span class="label label-important">Place Non Réservée</span>
<?php
	 }
?><br /><br /><br />
	
	<script type="text/javascript">
		$('#player<?php echo $nb_j ?>-btn').click(function(){
			var p_prenom = $('#player<?php echo $nb_j ?>-prenom').val();
			var p_nom = $('#player<?php echo $nb_j ?>-nom').val();
			var p_pseudo = $('#player<?php echo $nb_j ?>-pseudo').val();
			var p_email = $('#player<?php echo $nb_j ?>-email').val();
      var hasError = false;
      if(p_prenom == "Prénom" || p_prenom==''){
        $('#player<?php echo $nb_j ?>-prenom').css('background-color','#F2BABA');
        hasError = true;
      } else {
        $('#player<?php echo $nb_j ?>-prenom').css('background-color','#E6FFB8');
      }
      if(p_nom == "Nom" || p_prenom==''){
        $('#player<?php echo $nb_j ?>-nom').css('background-color','#F2BABA');
        hasError = true;
      } else {
        $('#player<?php echo $nb_j ?>-nom').css('background-color','#E6FFB8');
      }
      if(p_pseudo == "Pseudo" || p_prenom==''){
        $('#player<?php echo $nb_j ?>-pseudo').css('background-color','#F2BABA');
        hasError = true;
      } else {
        $('#player<?php echo $nb_j ?>-pseudo').css('background-color','#E6FFB8');
      }
      if(p_email == "Email" || p_prenom==''){
        $('#player<?php echo $nb_j ?>-email').css('background-color','#F2BABA');
        hasError = true;
      } else {
        $('#player<?php echo $nb_j ?>-email').css('background-color','#E6FFB8');
      }
      if(hasError){
        alert('Merci de remplir tous les champs indiqués en rouge !');
      } else {
        $('#player<?php echo $nb_j ?>-btn').attr('value','Envoi...');
        $('#player<?php echo $nb_j ?>-btn').unbind('click');
        $(window).unbind('beforeunload');
			  $.ajax({
              	url: "<?php echo url_for('@ajax_team_users?id='.$team->getId()) ?>?equipe_action=update&user=<?php echo $player->getId() ?>&prenom="+p_prenom+"&nom="+p_nom+"&pseudo="+p_pseudo+"&email="+p_email,
	              cache: false,
	              success: function(html){
	              	 if(html == "-1"){
	              			alert('Nom et prénom nécessaires !');
	              	 } else {
											$(window).attr('location','');
	              	 }
	              },
	              error: function(){
			            alert('Une erreur est survenue (merci d\'actualiser la page puis réesayer).');
	              }
	        });
      }
		});
	</script>
<?php
		$nb_j++;
	}
	
	for($nb_j; $nb_j<=$maxplayers; $nb_j++){
?>

	<input class="examplify_labels" type="text" style="width:180px" id="player<?php echo $nb_j ?>-pseudo" title="Pseudo">
	<input class="examplify_labels" type="text" style="width:280px" size="50" id="player<?php echo $nb_j ?>-email" title="Email"><br />
	<input type="text" class="examplify_labels" style="width:180px" id="player<?php echo $nb_j ?>-prenom" title="Prénom">
	<input class="examplify_labels" type="text" style="width:160px" id="player<?php echo $nb_j ?>-nom" title="Nom">
	<input type="button" id="player<?php echo $nb_j ?>-btn" class="btn-primary" value="Ajouter"> <span class="label label-warning">Joueur manquant</span>
	<br />
  <br />
  <br />
	
	<script type="text/javascript">
		$('#player<?php echo $nb_j ?>-btn').click(function(){
			var p_prenom = $('#player<?php echo $nb_j ?>-prenom').val();
			var p_nom = $('#player<?php echo $nb_j ?>-nom').val();
			var p_pseudo = $('#player<?php echo $nb_j ?>-pseudo').val();
			var p_email = $('#player<?php echo $nb_j ?>-email').val();
      var hasError = false;
      if(p_prenom == "Prénom" || p_prenom==''){
        $('#player<?php echo $nb_j ?>-prenom').css('background-color','#F2BABA');
        hasError = true;
      } else {
        $('#player<?php echo $nb_j ?>-prenom').css('background-color','#E6FFB8');
      }
      if(p_nom == "Nom" || p_prenom==''){
        $('#player<?php echo $nb_j ?>-nom').css('background-color','#F2BABA');
        hasError = true;
      } else {
        $('#player<?php echo $nb_j ?>-nom').css('background-color','#E6FFB8');
      }
      if(p_pseudo == "Pseudo" || p_prenom==''){
        $('#player<?php echo $nb_j ?>-pseudo').css('background-color','#F2BABA');
        hasError = true;
      } else {
        $('#player<?php echo $nb_j ?>-pseudo').css('background-color','#E6FFB8');
      }
      if(p_email == "Email" || p_prenom==''){
        $('#player<?php echo $nb_j ?>-email').css('background-color','#F2BABA');
        hasError = true;
      } else {
        $('#player<?php echo $nb_j ?>-email').css('background-color','#E6FFB8');
      }
      if(hasError){
        alert('Merci de remplir tous les champs indiqués en rouge !');
      } else {
        $('#player<?php echo $nb_j ?>-btn').attr('value','Envoi...');
        $('#player<?php echo $nb_j ?>-btn').unbind('click');
        $(window).unbind('beforeunload');
			  $.ajax({
              	url: "<?php echo url_for('@ajax_team_users?id='.$team->getId()) ?>?equipe_action=add&prenom="+p_prenom+"&nom="+p_nom+"&pseudo="+p_pseudo+"&email="+p_email,
	              cache: false,
	              success: function(html){
	              	 if(html){
		              	 if(html == "-1"){
		              			alert('Nom et prénom nécessaires !');
		              	 } else {
												$(window).attr('location','');
		              	 }
		             } else {
				            alert('Désolé, le nombre maximum de joueurs à été atteint.');
	              	 }
	              },
	              error: function(){
			            alert('Une erreur est survenue (merci d\'actualiser la page puis réesayer).');
	              }
	        });
      }
		});
	</script>
<?php
	}
?>
</p>
<script type="text/javascript">
	$('.examplify_labels').example(function() {
	  return $(this).attr('title');
	}, { className: 'grey_labelify' });
</script>
<?php if($many_payed): ?>
<div class="alert alert-success">
	<?php echo $team->setSlotReserved(1)->save() ?>
	Les places des membres de cette équipe sont maintenant officiellement réservées.
</div>
<?php else: ?>
<div class="alert alert-error">
	<?php echo $team->setSlotReserved(0)->save() ?>
	Les places des membres de cette équipe NE sont PAS encore réservées.
</div>
<script type="text/javascript">
  $(window).bind('beforeunload', function(){
    return 'Attention: Les places des membres de cette équipe NE sont PAS encore réservées.';
  });
</script>
<div class="alert alert-info">
Note: Afin que votre place soit réservée, vous devez vous devez vous acquitter des frais d'entrée de votre équipe en remplissant les champs, en cliquant sur "Ajouter" puis sur "Payer" pour chacun des membres.
</div>
<?php endif; ?>
</div>