<div class="boxed">
	<h2>Participants - <?php echo $step->getName() ?></h2>
  <?php foreach($rounds as $round): ?>
    <div style="margin:30px;float:left;width:150px;overflow:hidden;">
    <?php echo image_tag($round->getGame()->getImage()) ?><br />
    <?php echo count($round->getTeamReserved()) ?>/<?php echo $round->getGame()->getMaxNbTeams() ?> Equipe(s) réservées<br />
    <?php echo $round->getNbTeamWaitingPayment() ?> Equipe(s) en attente de paiement<br />
    <?php foreach($round->getTeamReserved() as $team): ?>
      <span title="<?php echo $team ?>">
        <?php echo truncate_text($team,20) ?>:
      </span><br />
      <div style="padding-left:10px;">
      <?php foreach($team->getPlayer() as $player): ?>
        <span title="<?php echo $player->getPseudo() ?>">
          <?php echo truncate_text('- '.$player->getPseudo(),20) ?>
        </span><br />
      <?php endforeach; ?>
      </div>
    <?php endforeach; ?>
    </div>
  <?php endforeach; ?>
  <div class="clear"></div>
</div>
<?php if($step->getStatus()==1): ?>
<p style="text-align: center; padding-bottom: 40px; margin-top:20px;">
  <a href="<?php echo url_for('participation_choix_jeu',$step) ?>" class="btn btn-primary btn-larger">Inscription (20€/joueur)</a>
</p>
<?php endif; ?>