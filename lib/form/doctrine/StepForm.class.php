<?php

/**
 * Step form.
 *
 * @package    SupLan
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class StepForm extends BaseStepForm
{
  public function configure()
  {
  	$this->widgetSchema['status'] = new sfWidgetFormSelect(array('choices' => array('1'=>'Ouvert à la réservation','0'=>'Réservation fermée')));
    $this->widgetSchema['content'] = new sfWidgetFormTextareaTinyMCE();
    $this->widgetSchema['content_schedule'] = new sfWidgetFormTextareaTinyMCE();
    $this->widgetSchema['content_hotel'] = new sfWidgetFormTextareaTinyMCE();
    $this->widgetSchema['content_access'] = new sfWidgetFormTextareaTinyMCE();
  	unset($this['created_at'], $this['updated_at'], $this['slug']);
  }
}
